/* eslint-disable max-len */

exports.seed = knex => knex('user').insert([
    { id: 1, roleId: 1, groupId: 1, email: 'test@test.com.tw', name: 'test', password: '$argon2id$v=19$m=4096,t=3,p=1$y56e/BaKnnXEkB/hZa9l6Q$6kuIVE1aXTWJOCP4vJ49rEI8VbIXxPEXDhxbt3+ckUw', root: 1 },
    { id: 2, roleId: 2, groupId: 1, email: 'test1@test.com.tw', name: 'test1', password: '$argon2id$v=19$m=4096,t=3,p=1$y56e/BaKnnXEkB/hZa9l6Q$6kuIVE1aXTWJOCP4vJ49rEI8VbIXxPEXDhxbt3+ckUw', root: 0 },
    { id: 3, roleId: 3, groupId: 2, email: 'test2@test.com.tw', name: 'test2', password: '$argon2id$v=19$m=4096,t=3,p=1$y56e/BaKnnXEkB/hZa9l6Q$6kuIVE1aXTWJOCP4vJ49rEI8VbIXxPEXDhxbt3+ckUw', root: 0 },
    { id: 4, roleId: 4, groupId: 2, email: 'test3@test.com.tw', name: 'test3', password: '$argon2id$v=19$m=4096,t=3,p=1$y56e/BaKnnXEkB/hZa9l6Q$6kuIVE1aXTWJOCP4vJ49rEI8VbIXxPEXDhxbt3+ckUw', root: 0 },
]);
