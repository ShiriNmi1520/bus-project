/**
 * permission defined rules:
 */
const table = 'permission';
const permissions = [
  'user.get',
  'user.create',
  'user.update',
  'user.delete',
  'user.permission',
  'role.get',
  'role.create',
  'role.update',
  'role.delete',
  'permission.get',
  'subscribe.get',
  'subscribe.create',
  'subscribe.delete'
];

exports.permissions = permissions;

exports.seed = async (knex) => {
  const permissionRows = await knex(table);
  const insertPermissions = permissions.filter((permission) => permissionRows.findIndex((row) => row.name === permission) === -1);
  const deletePermissions = permissionRows.filter((permissionRow) => permissions.indexOf(permissionRow.name) === -1);
  const promise = [];
  if (insertPermissions.length > 0) {
    promise.push(knex(table).insert(insertPermissions.map((permission) => ({ name: permission }))));
  }
  if (deletePermissions.length > 0) {
    promise.push(knex(table).del().whereIn('name', deletePermissions.map((permission) => permission.name)));
  }
  return Promise.all(promise);
};
