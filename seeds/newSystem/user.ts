import argon2 from 'argon2';
import logger from '../../src/models/log';
import { Knex } from 'knex';

exports.seed = (knex: Knex) => seed(knex);

function seed(knex: Knex) {
  const email = 'int@int.com.tw';
  const password = 'admin_01';

  return argon2.hash(password).then((hash) => knex('user').insert([
    {
      roleId: 1, groupId: 1, email, name: 'Default Admin', password: hash
    },
  ]).then(() => {
    logger.info('email: int@int.com.tw');
    logger.info(`password: ${password}`);
  }));
}
