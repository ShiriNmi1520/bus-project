
exports.seed = knex => knex('role').del().then(() => knex('role').insert([
    { id: 1, groupId: 1, name: 'Manager', permissions: '["*"]' },
]));
