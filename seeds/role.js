exports.seed = knex => knex('role').insert([
    { id: 1, groupId: 1, name: 'Manager', permissions: '["*"]' },
    { id: 2, groupId: 1, name: 'User', permissions: '[]' },
    { id: 3, groupId: 2, name: 'Manager', permissions: '["*"]' },
    { id: 4, groupId: 2, name: 'User', permissions: '[]' },
]);
