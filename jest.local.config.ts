import { JestConfigWithTsJest } from 'ts-jest';

const jestConfig: JestConfigWithTsJest = {
    'preset': 'ts-jest',
    'testEnvironment': 'node',
    'collectCoverage': true,
    'collectCoverageFrom': [
        'src/**/*.{js,ts}',
    ],
    'coverageReporters': ['text'],
    'modulePathIgnorePatterns': ['__mocks__', 'utils'],
};

export default jestConfig;
