
exports.up = function(knex) {
    return knex.schema.hasTable('systemLog').then(exists => {
        if (!exists) {
            return knex.schema.createTable('systemLog', function(table) {
                table.increments('id');
                table.json('request').defaultTo('{}');
                table.integer('responseCode');
                table.integer('userId')
                    .unsigned()
                    .references('id').inTable('user').onUpdate('CASCADE').onDelete('SET NULL');
                table.json('userRaw').defaultTo('{}');
                table.timestamp('createdAt').defaultTo(knex.fn.now());
            });
        }
    });
};

exports.down = function(knex) {
    knex.schema.dropTableIfExists('systemLog');
};
