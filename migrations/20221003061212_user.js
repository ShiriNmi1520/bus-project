exports.up = function (knex) {
  return knex.schema.hasTable('user').then((exists) => {
    if (!exists) {
      return knex.schema.createTable('user', (table) => {
        table.increments('id');
        table.string('name').notNullable();
        table.integer('roleId')
          .notNullable()
          .unsigned()
          .references('id')
          .inTable('role')
          .onUpdate('CASCADE')
          .onDelete('CASCADE');
        table.integer('groupId')
          .notNullable()
          .unsigned()
          .references('id')
          .inTable('group')
          .onUpdate('CASCADE')
          .onDelete('CASCADE');
        table.string('email').notNullable();
        table.string('password', 255).notNullable();
        table.boolean('root').defaultTo(0);
        table.string('resetToken');
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
      });
    }
  });
};

exports.down = function (knex) {
  knex.schema.dropTableIfExists('user');
};
