
exports.up = function(knex) {
    return knex.schema.hasTable('role').then(exists => {
        if (!exists) {
            return knex.schema.createTable('role', function(table) {
                table.increments('id');
                table.string('name').notNullable();
                table.integer('groupId')
                    .notNullable()
                    .unsigned()
                    .references('id').inTable('group').onUpdate('CASCADE').onDelete('CASCADE');
                table.json('permissions').defaultTo('[]');
                table.timestamp('createdAt').defaultTo(knex.fn.now());
                table.timestamp('updatedAt').defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            });
        }
    });
};

exports.down = function(knex) {
    knex.schema.dropTableIfExists('role');
};
