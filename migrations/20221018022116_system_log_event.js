
exports.up = function(knex) {
    return knex.raw('DROP EVENT IF EXISTS event_system_log;').then(() => {
        return knex.raw(`
CREATE EVENT event_system_log ON SCHEDULE EVERY 1 DAY ON COMPLETION NOT PRESERVE ENABLE
DO
    DELETE FROM system_log WHERE createdAt < DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 3 MONTH);`);
    });
};

exports.down = function(knex) {
    return knex.raw(`DROP EVENT IF EXISTS event_system_log;`);
};
