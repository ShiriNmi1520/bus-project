exports.up = function (knex) {
  return knex.schema.hasTable('subscribe').then((exists) => {
    if (!exists) {
      return knex.schema.createTable('subscribe', (table) => {
        table.string('id');
        table.string('email')
          .nullable();
        table.string('clientID')
          .nullable();
        table.string('busNo')
          .notNullable();
        table.string('station')
          .notNullable();
        table.integer('minutesBefore')
          .notNullable();
        table.string('status')
          .notNullable();
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
      });
    }
  });
};

exports.down = function (knex) {
  knex.schema.dropTableIfExists('subscribe');
};
