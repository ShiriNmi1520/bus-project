
exports.up = function(knex) {
    return knex.schema.hasTable('group').then(exists => {
        if (!exists) {
            return knex.schema.createTable('group', function(table) {
                table.increments('id');
                table.string('name').notNullable();
                table.timestamp('createdAt').defaultTo(knex.fn.now());
                table.timestamp('updatedAt').defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            });
        }
    });
};

exports.down = function(knex) {
    knex.schema.dropTableIfExists('group');
};
