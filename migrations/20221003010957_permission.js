
exports.up = function(knex) {
    return knex.schema.hasTable('permission').then(exists => {
        if (!exists) {
            return knex.schema.createTable('permission', function(table) {
                table.string('name').primary();
                table.timestamp('createdAt').defaultTo(knex.fn.now());
            });
        }
    });
};

exports.down = function(knex) {
    knex.schema.dropTableIfExists('permission');
};
