DC=docker-compose
MC=api
UTC=unitary-test

OS := $(shell uname -s)
ifeq ($(OS),Darwin)
	FIX_SED=""
	FIX_TR=LC_CTYPE=C
endif

all:start

init: ## Init the project after a git clone
	@echo "INIT PROJECT"
	@echo "Setup gitlab registry"
	@echo @brilltek42:registry=https://gitlab.com/api/v4/packages/npm/ > .npmrc
	@echo //gitlab.com/api/v4/projects/41349200/packages/npm/:_authToken=hAksTMsS4qXLzjT7wfKX >> .npmrc
	@echo //gitlab.com/api/v4/packages/npm/:_authToken=hAksTMsS4qXLzjT7wfKX >> .npmrc
	@echo "Copying .env.dist in .env"
	@cp .env.dist .env
	@sed -i $(FIX_SED) "s@SECRET_TO_CHANGE@`$(FIX_TR) tr -dc A-Za-z0-9_ < /dev/urandom | head -c 8192 | xargs`@g" .env
	@sed -i $(FIX_SED) "s@NODE_ENV=@NODE_ENV=development@g" .env
	@echo ".env: \n"
	@cat .env
	@echo "\n"
	@echo "Copying .env in .env.test"
	@cp .env .env.test
	@sed -i $(FIX_SED) "s@NODE_ENV=development@NODE_ENV=test@g" .env.test
	@echo "Copying .env.mysql.dist in .env.mysql"
	@cp .env.mysql.dist .env.mysql
	@echo "\n"

dev: ## Launch the project stack with attached output (ctrl+c will kill project)
	@echo "Launch attached project and build\n"
	$(DC) up --build

build: ## Build the project stack
	@echo "build\n"
	$(DC) build
start: ## Build and launch the project in background
	@echo "Launch dettached projet and build\n"
	$(DC) up -d --build
stop: ## Stop the project stack
	$(DC) stop

clean: ## Stop and delete the project stack
	$(DC) down
tree: ## Make a tree view of the project folder
	@tree | sed 's/├/\+/g; s/─/-/g; s/└/\\/g'
logs: ## Attach to standard output of containers (to see logs)
	$(DC) -f docker-compose.yml logs -f $(MC)

re: clean start

exec: ## Execute command inside api container, need to use command=
	$(DC) exec $(MC) $(command)

migrate_new: ## Create new migration inside /migrations folder, need to use name=
	$(DC) exec $(MC) ./node_modules/knex/bin/cli.js migrate:make $(name)

seed_new: ## Create new seed inside /seeds folder, need to use name=
	$(DC) exec $(MC) ./node_modules/knex/bin/cli.js seed:make $(name)

test: build_test run_test

build_test: ## Launch test stack
	@echo "Launch test\n"
	$(DC) -f docker-compose.test.yml up -d --build

run_test: ## Run unitary test, option file (only run assign file)
	$(DC) -f docker-compose.test.yml exec $(UTC) npm run test:watch -- $(file)

clean_test: ## Stop and delete the test stack
	@echo "Remove test\n"
	$(DC) -f docker-compose.test.yml down

logs_test: ## Attach to standard output of test containers (to see logs)
	@echo "Attach logs\n"
	$(DC) -f docker-compose.test.yml logs -f $(UTC)

re_test: clean_test test

lint: ## Launch eslint in fix mode on api container
	$(DC) -f docker-compose.yml exec $(MC) ./node_modules/eslint/bin/eslint.js --fix .

help: ## help command
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m- %-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := start

install_dc: ## Install docker
	sudo curl https://get.docker.com | sudo sh -

install_dcc: ## Install docker-compose
	COMPOSE_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
	sudo sh -c "curl -L https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
	sudo chmod +x /usr/local/bin/docker-compose
	sudo sh -c "curl -L https://raw.githubusercontent.com/docker/compose/${COMPOSE_VERSION}/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose"
	docker-compose -v

.PHONY: all test clean_test
