# template_api
The api of Template

# Express

Express	is used	to powered API Routes

You need to make a route folder or file by entity in `routes` directory and include it in `webserver.js`

# Express-validator

Express-validator is used as middleware to validate payload send by users to avoid verify in model function directly.
You can also sanitize the payload data.

Documentation is available [here](https://express-validator.github.io/docs/)

# Swagger

Swagger	is used	to powered the API Documentation on `/api-docs` route.

Swagger	information are	write in comments directly in code.
Maybe we will moving later to a	dedicated JSON file (not sure).

Currently we use version 3

Documentation is available [here](https://swagger.io/specification/)

# Eslint

ESLint statically analyzes your code to quickly find problems.

Recommend to install ESLint in Vscode Extensions. 

When `src/index.js` was started, will be automatic run the eslint.

# Knex
We are using `knex` as Query Builder.

Please use `migrations`, `seeds` and `mocks` (not implemented here yet) from this tool.

## Migrations

To make changes on DB, you need to add a migration: 
```sh 
knex migrate:make migration_name
```

To run migration locally:
```sh
knex migrate:latest
```

To un run migration locally:
```sh
knex migrate:rollback
```

## Seeds

You may need to have data in DB for testing.

To create a new seed:
```sh
knex seed:make seed_name
```

To fill the DB with all seeds: 
```sh
knex seed:run
```

*****

Knex is used also to make SQL Queries more easier and understandable without being high level like an ORM.

For more info about `knex`, you can checking [Knex Official Documentation](https://knexjs.org/guide/) or this [Cheatsheet](https://devhints.io/knex) 