const path = require('path');
const nodeExternals = require('webpack-node-externals');
const WebpackObfuscator = require('webpack-obfuscator');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    mode: 'production',
    target: 'node',
    entry: {
        app: ['./src/index.ts'],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'index.bundle.js',
    },
    plugins: [
        new WebpackObfuscator({
            rotateStringArray: true,
        }),
        new BundleAnalyzerPlugin({
            analyzerHost: '0.0.0.0',
            analyzerMode: 'static',
        }),
    ],
    externals: [
        nodeExternals(),
    ],
    optimization: {
        chunkIds: 'size',
        // method of generating ids for chunks
        moduleIds: 'size',
        // method of generating ids for modules
        mangleExports: 'size',
        // rename export names to shorter names
        minimize: true,
    },
};
