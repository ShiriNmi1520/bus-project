FROM node:18-alpine
WORKDIR /usr/src/app

#Git is dependency of Jest for watch parameter
RUN apk update
RUN apk --no-cache add tzdata git curl

#Install nodemon and ts-node Global
RUN yarn global add nodemon ts-node

# Copy package.json
COPY ./package* ./
COPY ./yarn.lock ./
COPY ./.npmrc ./

# Install dep
RUN yarn

CMD ["yarn", "dev"]
