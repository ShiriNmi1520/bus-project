import app from '../src/webserver';
import supertest from 'supertest';
import testTemplate from './utils/testTemplate';
import { init, login } from './utils';
import { db } from '../src/db/mysql';

let token = '';
const request = supertest(app);

beforeAll(async () => {
    await init();
    token = await login();
});

describe('/permissions', () => {
    describe('GET /permissions', () => {
        it('Success', async () => {
            const response = await request
                .get('/permissions')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(response.body.length).toBeGreaterThanOrEqual(0);
        });
        testTemplate.get.checkAuth('/permissions');
        testTemplate.get.checkPermission('/permissions');
    });
});


afterAll(done => {
    db.destroy();
    done();
});
