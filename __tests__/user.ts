import app from '../src/webserver';
import supertest from 'supertest';
import { faker } from '@faker-js/faker';
import { init, login } from './utils';
import { db } from '../src/db/mysql/index';
import testTemplate from './utils/testTemplate';
import mail from '../src/models/mail';
import { Options } from 'nodemailer/lib/mailer';

const request = supertest(app);
let resetToken: string | undefined = '';

jest.mock('nodemailer', () => ({
    createTransport: () => ({
        sendMail: (mailOptions: Options) => {
            const regex = (mailOptions.text as string).match('reset-password/(.*)');
            if (regex && regex.length >= 2) resetToken = regex[1];
            return Promise.resolve();
        },
        verify: () => Promise.resolve(),
        close: () => jest.fn(),
    }),
    createTestAccount: () => ({
        user: 'test',
        pass: 'test',
        smtp: {
            host: 'smtp.test.com',
            port: 456,
            secure: true,
        },
    }),
}));

let token = '';

beforeAll(async () => {
    await mail.init();
    await init();
    token = await login();
});

afterAll(done => {
    db.destroy();
    done();
});


describe('/users', () => {
    describe('GET /users', () => {
        it('Success', async () => {
            const response = await request
                .get('/users')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(response.body[0]).toHaveProperty('id');
            expect(response.body[0]).toHaveProperty('name');
            expect(response.body[0]).toHaveProperty('roleId');
            expect(response.body[0]).toHaveProperty('groupId');
            expect(response.body[0]).toHaveProperty('email');
        });
        testTemplate.get.checkAuth('/users');
        testTemplate.get.checkPermission('/users');
    });
    describe('GET /user/{id}', () => {
        it('Success', async () => {
            const response = await request
                .get('/user/1')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('id');
            expect(response.body).toHaveProperty('name');
            expect(response.body).toHaveProperty('roleId');
            expect(response.body).toHaveProperty('groupId');
            expect(response.body).toHaveProperty('email');
        });
        it('User Not Existing', async () => {
            const response = await request
                .get('/user/99999')
                .set('Authorization', token);
            expect(response.status).toBe(404);
        });
        testTemplate.get.checkAuth('/user/1');
        testTemplate.get.checkPermission('/user/1');
    });
    describe('GET /user/self/permission', () => {
        it('Success', async () => {
            const response = await request
                .get('/user/self/permission')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(response.body.length).toBeGreaterThanOrEqual(0);
        });
        testTemplate.get.checkAuth('/user/self/permission');
    });
    describe('POST /user', () => {
        let beforeLength = 0;
        beforeAll(async () => {
            const get = await request
                .get('/users')
                .set('Authorization', token);
            beforeLength = get.body.length;
        });
        it('Success', async () => {
            const email = faker.internet.email();
            const password = faker.internet.password();
            const response = await request
                .post('/user')
                .set('Authorization', token)
                .send({
                    name: faker.name.firstName(),
                    groupId: 1,
                    roleId: 2,
                    email,
                    password,
                });
            const get = await request
                .get('/users')
                .set('Authorization', token);
            const login = await request
                .post('/login')
                .send({
                    email,
                    password,
                });
            expect(response.status).toBe(200);
            expect(beforeLength + 1).toBe(get.body.length);
            expect(login.status).toBe(200);
            expect(login.body.id).toBe(response.body.id);
        });
        it('User Email Already Found', async () => {
            const response = await request
                .post('/user')
                .set('Authorization', token)
                .send({
                    name: faker.name.firstName(),
                    groupId: 1,
                    roleId: 2,
                    email: 'test1@test.com.tw',
                    password: faker.internet.password(),
                });
            expect(response.status).toBe(409);
        });
        testTemplate.post.checkAuth('/user');
        testTemplate.post.checkPermission('/user');
    });
    describe('PUT /user/{id}', () => {
        const email = faker.internet.email();
        const password = faker.internet.password();
        let id: number | undefined = undefined;
        beforeAll(async () => {
            const get = await request
                .get('/users')
                .set('Authorization', token);
            id = get.body.at(-1).id;
        });
        it('Success', async () => {
            const response = await request
                .put('/user/' + id)
                .set('Authorization', token)
                .send({
                    id,
                    name: faker.name.firstName(),
                    groupId: 1,
                    roleId: 2,
                    email,
                    password,
                });
            expect(response.status).toBe(200);
        });
        testTemplate.put.checkAuth('/user/' + id);
        testTemplate.put.checkPermission('/user/' + id);
    });
    describe('DELETE /user/{id}', () => {
        let id: number | undefined = undefined;
        beforeAll(async () => {
            const get = await request
                .get('/users')
                .set('Authorization', token);
            id = get.body.at(-1).id;
        });
        it('Success', async () => {
            const response = await request
                .delete('/user/' + id)
                .set('Authorization', token);
            expect(response.status).toBe(200);
        });
        testTemplate.delete.checkAuth('/user/' + id);
        testTemplate.delete.checkPermission('/user/' + id);
    });
    describe('POST /login', () => {
        it('Success', async () => {
            const response = await request
                .post('/login')
                .send({
                    email: 'test@test.com.tw',
                    password: 'test',
                    rememberMe: 1,
                });
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('id');
            expect(response.body).toHaveProperty('token');
        });
        it('Wrong Password', async () => {
            const response = await request
                .post('/login')
                .send({
                    email: 'test@test.com.tw',
                    password: faker.internet.password(),
                    rememberMe: 1,
                });
            expect(response.status).toBe(401);
        });
        it('Unknown email', async () => {
            const response = await request
                .post('/login')
                .send({
                    email: faker.internet.exampleEmail(),
                    password: faker.internet.password(),
                    rememberMe: 1,
                });
            expect(response.status).toBe(404);
        });
    });
});

describe('/user/reset', () => {
    describe('POST /user/reset', () => {
        it('Success', async () => {
            const response = await request
                .post('/user/reset')
                .send({
                    email: 'test1@test.com.tw',
                    host: 'http://test.com.tw',
                });
            expect(response.status).toBe(200);
        });
    });
    describe('GET /user/reset/verify/{token}', () => {
        it('Success', async () => {
            const response = await request
                .get('/user/reset/verify/' + resetToken)
                .query({
                    email: 'test1@test.com.tw',
                });
            expect(response.status).toBe(200);
        });
        it('Wrong token', async () => {
            const response = await request
                .get('/user/reset/verify/' + faker.internet.password())
                .query({
                    email: 'test1@test.com.tw',
                });
            expect(response.status).toBe(401);
        });
    });
    describe('POST /user/reset/{token}', () => {
        it('Success', async () => {
            const response = await request
                .post('/user/reset/' + resetToken)
                .send({
                    email: 'test1@test.com.tw',
                    password: faker.internet.password(),
                });
            expect(response.status).toBe(200);
        });
        it('Wrong token', async () => {
            const wrongToken = await request
                .post('/user/reset/' + faker.internet.password())
                .send({
                    email: 'test1@test.com.tw',
                    password: faker.internet.password(),
                });
            expect(wrongToken.status).toBe(401);
        });
    });
});
