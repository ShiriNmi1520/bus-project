import app from '../../src/webserver';
import supertest from 'supertest';
const request = supertest(app);

const base = (method: 'get' | 'post' | 'put' | 'delete') => {
    return {
        checkAuth: (api: string) => {
            it('check Authorization', async () => {
                const response = await request[method](api);
                expect(response.status).toBe(401);
            });
        },
        checkPermission: (api: string) => {
            it('check Permission', async () => {
                const getTestToken = await request
                    .post('/login')
                    .send({ email: 'test1@test.com.tw', password: 'test' });
                expect(getTestToken.status).toBe(200);

                const response = await request[method](api)
                    .set('Authorization', getTestToken.body.token);
                expect(response.status).toBe(403);
            });
        },
    };
};

const testTemplate = {
    get: base('get'),
    post: base('post'),
    put: base('put'),
    delete: base('delete'),
};

export default testTemplate;
