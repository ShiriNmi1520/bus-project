import app from '../../src/webserver';
import supertest from 'supertest';
const request = supertest(app);

export const login = () => request
    .post('/login')
    .send({ email: 'test@test.com.tw', password: 'test' })
    .then(response => response.body.token);
