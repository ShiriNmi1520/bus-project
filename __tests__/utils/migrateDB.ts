import { db } from '../../src/db/mysql';
import config from '../../src/config';
import knex, { Knex } from 'knex';

const checkMigrationInit = async (knexDB: Knex) => {
    return await knexDB.migrate.currentVersion() !== 'none';
};

const truncateTable = async (knexDB: Knex) => {
    const tables = await knexDB('INFORMATION_SCHEMA.TABLES')
        .pluck('TABLE_NAME')
        .where({ table_schema: knexDB.client.database() })
        .whereNotIn('TABLE_NAME', ['knex_migrations', 'knex_migrations_lock']);
    return Promise.all(tables.map(table => knexDB(table).truncate()));
};

export const init = async () => {
    if (!await checkMigrationInit(db)) {
        // Drop Old Database
        const rootDB = knex({
            client: 'mysql',
            connection: {
                host: config.mysql.host,
                user: 'root',
                port: config.mysql.port,
                password: process.env.MYSQL_ROOT_PASSWORD,
                database: config.mysql.database,
            },
        });
        await rootDB.raw('DROP DATABASE IF EXISTS ' + config.mysql.database);
        await rootDB.raw('CREATE DATABASE ' + config.mysql.database);
        await db.migrate.latest({ directory: './migrations' });
        rootDB.destroy();
    }
    await db.transaction(async trx => {
        await trx.raw('SET FOREIGN_KEY_CHECKS = 0;');
        await truncateTable(trx);
        await db.seed.run({ directory: './seeds' });
        await trx.raw('SET FOREIGN_KEY_CHECKS = 1;');
    });
    return true;
};
