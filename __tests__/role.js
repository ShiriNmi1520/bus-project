const app = require('../src/webserver');
const supertest = require('supertest');
const request = supertest(app);
const faker = require('faker');
const { init, login } = require('./utils');
const { db } = require('../src/db/mysql/index');
const { testTemplate } = require('./utils/testTemplate');

let token = '';
const template = testTemplate(request);

beforeAll(async () => {
    await init();
    token = await login();
});

afterAll(done => {
    db.destroy();
    done();
});


describe('/roles', () => {
    describe('GET /roles', () => {
        it('Success', async () => {
            const response = await request
                .get('/roles')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(response.body[0]).toHaveProperty('id');
            expect(response.body[0]).toHaveProperty('name');
            expect(response.body[0]).toHaveProperty('permissions');
            expect(Array.isArray(response.body[0].permissions)).toBe(true);
        });
        template.get.checkAuth('/roles');
        template.get.checkPermission('/roles');
    });
    describe('GET /role/{id}', () => {
        it('Success', async () => {
            const response = await request
                .get('/role/1')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('id');
            expect(response.body).toHaveProperty('name');
            expect(response.body).toHaveProperty('permissions');
            expect(Array.isArray(response.body.permissions)).toBe(true);
        });
        it('role Not Existing', async () => {
            const response = await request
                .get('/role/99999')
                .set('Authorization', token);
            expect(response.status).toBe(404);
        });
        template.get.checkAuth('/role/1');
        template.get.checkPermission('/role/1');
    });
    describe('POST /role', () => {
        let beforeLength = 0;
        beforeAll(async () => {
            const get = await request
                .get('/roles')
                .set('Authorization', token);
            beforeLength = get.body.length;
        });
        it('Success', async () => {
            const response = await request
                .post('/role')
                .set('Authorization', token)
                .send({
                    name: faker.name.firstName(),
                    permissions: ['user.*'],
                });
            const get = await request
                .get('/roles')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(beforeLength + 1).toBe(get.body.length);
        });
        it('Role Name Already Found', async () => {
            const response = await request
                .post('/role')
                .set('Authorization', token)
                .send({
                    name: 'Manager',
                    permissions: ['user.*'],
                });
            expect(response.status).toBe(409);
        });
        template.post.checkAuth('/role');
        template.post.checkPermission('/role');
    });
    describe('PUT /role/{id}', () => {
        let id = null;
        beforeAll(async () => {
            const get = await request
                .get('/roles')
                .set('Authorization', token);
            id = get.body.at(-1).id;
        });
        it('Success', async () => {
            const response = await request
                .put('/role/' + id)
                .set('Authorization', token)
                .send({
                    name: faker.name.firstName(),
                    permissions: ['user.*'],
                });
            expect(response.status).toBe(200);
        });
        template.put.checkAuth('/role/' + id);
        template.put.checkPermission('/role/' + id);
    });
    describe('DELETE /role/{id}', () => {
        let id = null;
        beforeAll(async () => {
            const get = await request
                .get('/roles')
                .set('Authorization', token);
            id = get.body.at(-1).id;
        });
        it('Success', async () => {
            const response = await request
                .delete('/role/' + id)
                .set('Authorization', token);
            expect(response.status).toBe(200);
        });
        it('Can not delete used role', async () => {
            const response = await request
                .delete('/role/' + 1)
                .set('Authorization', token);
            expect(response.status).toBe(403);
        });
        template.delete.checkAuth('/role/' + id);
        template.delete.checkPermission('/role/' + id);
    });
});
