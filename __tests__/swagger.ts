import app from '../src/webserver';
import supertest from 'supertest';
import init from '../src/models/swagger';

const request = supertest(app);

beforeAll(() => {
    init(app);
});

afterAll(done => {
    done();
});


describe('/swagger.json', () => {
    it('Success', async () => {
        const response = await request
            .get('/swagger.json');
        expect(response.status).toBe(200);
        expect(Object.keys(response.body.paths).length).toBeGreaterThanOrEqual(1);
    });
});
