CREATE DATABASE client1;
CREATE DATABASE client2;
CREATE DATABASE client3;

CREATE USER 'test'@'%' IDENTIFIED BY 'test';

GRANT ALL PRIVILEGES ON *.* TO 'test'@'%';
