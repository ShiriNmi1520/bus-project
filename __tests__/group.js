const app = require('../src/webserver');
const supertest = require('supertest');
const request = supertest(app);
const faker = require('faker');
const { init, login } = require('./utils');
const { db } = require('../src/db/mysql/index');
const testTemplate = require('./utils/testTemplate');

let token = '';
const template = testTemplate(request);

beforeAll(async () => {
    await init();
    token = await login();
});

afterAll(done => {
    db.destroy();
    done();
});

describe('/groups', () => {
    describe('GET /groups', () => {
        it('Success', async () => {
            const response = await request
                .get('/groups')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(response.body[0]).toHaveProperty('id');
            expect(response.body[0]).toHaveProperty('name');
        });
        template.get.checkAuth('/groups');
        template.get.checkPermission('/groups');
    });
    describe('GET /group/{id}', () => {
        it('Success', async () => {
            const response = await request
                .get('/group/1')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('id');
            expect(response.body).toHaveProperty('name');
        });
        it('Group Not Existing', async () => {
            const response = await request
                .get('/group/99999')
                .set('Authorization', token);
            expect(response.status).toBe(404);
        });
        template.get.checkAuth('/group/1');
        template.get.checkPermission('/group/1');
    });
    describe('POST /group', () => {
        let beforeLength = 0;
        beforeAll(async () => {
            const get = await request
                .get('/groups')
                .set('Authorization', token);
            beforeLength = get.body.length;
        });
        it('Success', async () => {
            const response = await request
                .post('/group')
                .set('Authorization', token)
                .send({
                    name: faker.name.firstName(),
                });
            const get = await request
                .get('/groups')
                .set('Authorization', token);
            expect(response.status).toBe(200);
            expect(beforeLength + 1).toBe(get.body.length);
        });
        it('Group Name Already Found', async () => {
            const response = await request
                .post('/group')
                .set('Authorization', token)
                .send({
                    name: 'brilltek',
                });
            expect(response.status).toBe(409);
        });
        template.post.checkAuth('/group');
        template.post.checkPermission('/group');
    });
    describe('PUT /group/{id}', () => {
        let id = null;
        beforeAll(async () => {
            const get = await request
                .get('/groups')
                .set('Authorization', token);
            id = get.body.at(-1).id;
        });
        it('Success', async () => {
            const response = await request
                .put('/group/' + id)
                .set('Authorization', token)
                .send({
                    id,
                    name: faker.name.firstName(),
                });
            expect(response.status).toBe(200);
        });
        template.put.checkAuth('/group/' + id);
        template.put.checkPermission('/group/' + id);
    });
    describe('DELETE /group/{id}', () => {
        let id = null;
        beforeAll(async () => {
            const get = await request
                .get('/groups')
                .set('Authorization', token);
            id = get.body.at(-1).id;
        });
        it('Success', async () => {
            const response = await request
                .delete('/group/' + id)
                .set('Authorization', token);
            expect(response.status).toBe(200);
        });
        template.delete.checkAuth('/group/' + id);
        template.delete.checkPermission('/group/' + id);
    });
});
