import { JestConfigWithTsJest } from 'ts-jest';

const jestConfig: JestConfigWithTsJest = {
    'preset': 'ts-jest',
    'testEnvironment': 'node',
    'collectCoverage': true,
    'collectCoverageFrom': [
        'src/**/*.{js,ts}',
    ],
    'coverageDirectory': 'coverage',
    'coverageReporters': ['cobertura', 'text', 'html', 'lcovonly'],
    'reporters': ['default', 'jest-junit'],
    'modulePathIgnorePatterns': ['__mocks__', 'utils'],
};

export default jestConfig;
