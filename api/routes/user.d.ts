import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';

export interface PostUserCreateRequestBody {
    name: string
    groupId: number
    roleId: number
    email: string
    password: string
}
export interface PostUserCreateResponse {
    /** user id */
    id: number | undefined
}

export interface PostUserLoginRequestBody {
    email: string
    password: string
    rememberMe?: boolean
}
export interface PostUserLoginResponse {
    /** user id */
    id: number | undefined
    /** token */
    token: string
}

export interface PostUserResetRequestBody {
    email: string
    host: string
}
export interface PostUserResetResponse {
    msg: string
}

export interface PostUserResetTokenRequestParams extends ParamsDictionary {
    token: string
}
export interface PostUserResetTokenRequestBody {
    email: string
    password: string
}
export interface PostUserResetTokenResponse {
    msg: string
}

export interface GetUserResetTokenVerifyRequestParams extends ParamsDictionary {
    token: string
}
export interface GetUserResetTokenVerifyRequestQuery extends ParsedQs {
    email: string
}
export interface GetUserResetTokenVerifyResponse {
    msg: string
}

export interface GetUserRequestParams extends ParamsDictionary {
    id: number
}
export interface GetUserResponse {
    id: number
    name: string
    roleId: number
    groupId: number
    email: string
}

export type GetUsersResponse = Array<GetUserResponse>

export interface PutUserRequestParams extends ParamsDictionary {
    id: number
}
export interface PutUserRequestBody {
    roleId: number
    groupId: number
    name: string
    email: string
    password: string
}
export interface PutUserResponse {
    msg: string
}

export interface DeleteUserRequestParams extends ParamsDictionary {
    id: number
}
export interface DeleteUserResponse {
    msg: string
}
