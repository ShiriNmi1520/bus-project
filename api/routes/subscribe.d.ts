export interface PostSubscribeBusEmailNotifyRequestBody {
  email: string,
  busNo: string,
  station: string,
  minutesBefore: number,
}

export interface PostSubscribeBusMQTTNotifyRequestBody {
  clientID: string,
  busNo: string,
  station: string,
  minutesBefore: number
}

export interface PostSubscribeBusEmailNotifyResponse {
  id: string | undefined
  msg: string
}

export interface PostSubscribeBusMQTTNotifyResponse {
  id: string | undefined
  msg: string
}

export interface UnSubscribeBusEmailNotifyRequestBody {
  email: string,
  busNo: string,
  station: string
}

export interface UnSubscribeBusMQTTNotifyRequestBody {
  clientID: string,
  busNo: string,
  station: string
}

export interface UnSubscribeBusEmailNotifyResponse {
  msg: string
}

export interface UnSubscribeBusMQTTNotifyResponse {
  msg: string
}

export interface GetSubscribeStatusReqeustBody {
  subscribeID: string
}

export interface GetSubscribeStatus {
  id: string,
  status: Pick<'subscribed' | 'unsubscribed' | 'error'>,
  email: string | null,
  clientID: string | null,
  busNo: string | null,
  station: string | null
  minutesBefore: number
}

export type GetSubscribeStatusReponseBody = Array<GetSubscribeStatus>
