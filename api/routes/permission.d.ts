export interface GetPermissionResponse {
  name: string
}

export type GetPermissionsResponse = Array<GetPermissionResponse>
