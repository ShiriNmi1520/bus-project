import { param, query, body } from 'express-validator';
import { RouteFunction } from '../@types/routes';
import { db } from '../db/mysql';
const { table } = require('../db/mysql');
import { isAuth } from '../middleware';
import * as Api from '../../api';
import { isAlreadySubscribed } from "../services/subscribe";
import * as short from 'short-uuid';

export default ((app, validate) => {
  /**
   * @openapi
   * /subscribe:
   *   get:
   *     tags:
   *       - subscribe
   *     security:
   *       - token: []
   *     summary: get subscribe information
   *     responses:
   *       200:
   *         $ref: '#/components/responses/BaseResponseArrayString'
   */
  app.get<
    never,
    Api.GetSubscribeStatusReponseBody,
    Api.GetSubscribeStatusReqeustBody>('/subscribe', isAuth('subscribe.get'), validate([
  ]), (req, res, next) => {
    /**
     * @openapi
     * components:
     *   responses:
     *     GetSubscribeResponse:
     *       description: Success
     *       content:
     *         application/json:
     *           schema:
     *             type: object
     *             properties:
     *               name:
     *                 type: id
     *                 description: subscribe ID
     *                 example: 123
     *               status:
     *                 type: string
     *                 description: status of subscribe
     *                 example: subscribed
     *               busNo:
     *                 type: integer
     *                 description: subscribed bus number
     *                 example: 652
     *               station:
     *                 type: string
     *                 description: subscribed bus station name
     *                 example: 文心森林公園
     */
    db('subscribe').where(builder => {
      if (req.body.subscribeID !== undefined) builder.where({ id: req.body.subscribeID});
    }).then(subscribes => subscribes.map(subscribe => {
      const {
        id,
        email,
        clientID,
        busNo,
        station,
        minutesBefore,
        status
      } = subscribe;
      return { id, email, clientID, busNo, station, minutesBefore, status };
    })).then(rows => res.json(rows)).catch(next);
  });

  /**
   * @openapi
   * /subscribe/email:
   *   post:
   *     tags:
   *       - subscribe
   *     summary: subscribe bus notification via email
   *     requestBody:
   *       content:
   *         application/json:
   *           schema:
   *             $ref: '#/components/schemas/BusSubscribeEmailRequest'
   *     responses:
   *       200:
   *         $ref: '#/components/responses/BaseResponseMessage'
   *
   * components:
   *   schemas:
   *     BusSubscribeEmailRequest:
   *       type: object
   *       properties:
   *         email:
   *           type: string
   *         busNo:
   *           type: string
   *         station:
   *           type: string
   *         minutesBefore:
   *           type: integer
   */
  app.post<
    never,
    Api.PostSubscribeBusEmailNotifyResponse,
    Api.PostSubscribeBusEmailNotifyRequestBody>('/subscribe/email', isAuth('subscribe.create'), validate([
      body('email').isEmail().bail(),
      body('busNo').isString().bail(),
      body('minutesBefore').isInt().bail(),
      body('station').isString().bail()
        .custom((station, { req }) => isAlreadySubscribed(req.body.email, null, req.body.busNo, station))
  ]), (req, res, next) => {
    const {
      email,
      busNo,
      station,
      minutesBefore
    } = req.body;
    const promise = []
    const id = short.generate();
    promise.push(
      db('subscribe').insert({ id, email, clientID: null, busNo, station, minutesBefore, status: 'subscribed'})
    )

    Promise.all(promise).then(() =>
      res.json( { id, msg: 'Subscribed'})
    ).catch(next)
  });

  /**
   * @openapi
   * /subscribe/mqtt:
   *   post:
   *     tags:
   *       - subscribe
   *     summary: subscribe bus notification via MQTT
   *     requestBody:
   *       content:
   *         application/json:
   *           schema:
   *             $ref: '#/components/schemas/BusSubscribeMQTTRequest'
   *     responses:
   *       200:
   *         $ref: '#/components/responses/BaseResponseMessage'
   *
   * components:
   *   schemas:
   *     BusSubscribeMQTTRequest:
   *       type: object
   *       properties:
   *         clientID:
   *           type: string
   *         busNo:
   *           type: string
   *         station:
   *           type: string
   *         minutesBefore:
   *           type: integer
   */
  app.post<
    never,
    Api.PostSubscribeBusMQTTNotifyResponse,
    Api.PostSubscribeBusMQTTNotifyRequestBody>('/subscribe/mqtt', isAuth('subscribe.create'), validate([
    body('clientID').isEmail().bail(),
    body('busNo').isString().bail(),
    body('minutesBefore').isInt().bail(),
    body('station').isString().bail()
      .custom((station, { req }) => isAlreadySubscribed(null, req.body.clientID, req.body.busNo, station))
  ]), (req, res, next) => {
    const {
      clientID,
      busNo,
      station,
      minutesBefore
    } = req.body;
    const promise = []
    const id = short.generate();
    promise.push(
      db('subscribe').insert({ id, clientID, email: null, busNo, station, minutesBefore, status: 'subscribed'})
    )

    Promise.all(promise).then(() =>
      res.json( { id, msg: 'Subscribed'})
    ).catch(next)
  });

  /**
   * @openapi
   * /unsubscribe/email:
   *   post:
   *     tags:
   *       - subscribe
   *     summary: Unsubscribe bus notification via email
   *     requestBody:
   *       content:
   *         application/json:
   *           schema:
   *             $ref: '#/components/schemas/BusUnSubscribeEmailRequest'
   *     responses:
   *       200:
   *         $ref: '#/components/responses/BaseResponseMessage'
   *
   * components:
   *   schemas:
   *     BusUnSubscribeEmailRequest:
   *       type: object
   *       properties:
   *         email:
   *           type: string
   *         busNo:
   *           type: string
   *         station:
   *           type: string
   */
  app.delete<
    never,
    Api.UnSubscribeBusEmailNotifyResponse,
    Api.UnSubscribeBusEmailNotifyRequestBody>('/unsubscribe/email', isAuth('subscribe.delete'), validate([
  ]), (req, res, next) => {
    const {
      email,
      busNo,
      station
    } = req.body
    const promise = []

    promise.push(
      table.subscribe.update({ status: 'unsubscribed'}).where({ email, busNo, station})
    )

    Promise.all(promise).then(() => {
      res.json({ msg: 'Unsubscribe successful'})
    }).catch(next)
  });

  /**
   * @openapi
   * /unsubscribe/mqtt:
   *   post:
   *     tags:
   *       - subscribe
   *     summary: Unsubscribe bus notification via MQTT
   *     requestBody:
   *       content:
   *         application/json:
   *           schema:
   *             $ref: '#/components/schemas/BusUnSubscribeMQTTRequest'
   *     responses:
   *       200:
   *         $ref: '#/components/responses/BaseResponseMessage'
   *
   * components:
   *   schemas:
   *     BusUnSubscribeMQTTRequest:
   *       type: object
   *       properties:
   *         clientID:
   *           type: string
   *         busNo:
   *           type: string
   *         station:
   *           type: string
   */
  app.delete<
    never,
    Api.UnSubscribeBusMQTTNotifyResponse,
    Api.UnSubscribeBusMQTTNotifyRequestBody>('/unsubscribe/mqtt', isAuth('subscribe.delete'), validate([
  ]), (req, res, next) => {
    const {
      clientID,
      busNo,
      station
    } = req.body
    const promise = []

    promise.push(
      table.subscribe.update({ status: 'unsubscribed'}).where({ clientID, busNo, station})
    )

    Promise.all(promise).then(() => {
      res.json({ msg: 'Unsubscribe successful'})
    }).catch(next)
  });
}) as RouteFunction;
