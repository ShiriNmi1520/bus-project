import { RouteFunction } from '../@types/routes';
import { db } from '../db/mysql';
import { isAuth } from '../middleware';
import * as Api from '../../api';

export default ((app, validate) => {
    /**
      * @openapi
      * /permissions:
      *   get:
      *     tags:
      *       - permission
      *     security:
      *       - token: []
      *     summary: get all permissions
      *     responses:
      *       200:
      *         $ref: '#/components/responses/BaseResponseArrayString'
      */
    app.get<
    never,
    Api.GetPermissionsResponse>('/permissions', isAuth('permission.get'), validate([
    ]), (req, res, next) => {
        /**
         * @openapi
         * components:
         *   responses:
         *     GetPermissionResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               name:
         *                 type: string
         *                 description: permission scope
         *                 example: user.get
         */
        db('permission').then(permissions => permissions.map(permission => {
            const {
                name,
            } = permission;
            return { name };
        })).then(rows => res.json(rows)).catch(next);
    });
}) as RouteFunction;
