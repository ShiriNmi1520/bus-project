const { param, body } = require('express-validator');
const { table } = require('../db/mysql');
const { isAuth, isRoot } = require('../middleware');

module.exports = (app, validate) => {
    /**
     * @openapi
     * /groups:
     *   get:
     *     tags:
     *       - group
     *     security:
     *       - token: []
     *     summary: get all groups
     *     responses:
     *       200:
     *         $ref: '#/components/responses/GroupGetResponse'
     */

    app.get('/groups', isAuth(), isRoot, validate([]), (req, res, next) =>
        /**
         * @openapi
         * components:
         *   responses:
         *     GroupGetResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             type: array
         *             items:
         *               $ref: '#/components/schemas/GroupSchema'
         */
        table.group.get().then(rows => rows.map(row => {
            const {
                id,
                name,
            } = row;
            return { id, name };
        })).then(rows => res.json(rows)).catch(next)
    );

    /**
     * @openapi
     * /group/{id}:
     *   get:
     *     tags:
     *       - group
     *     security:
     *       - token: []
     *     summary: get group by id
     *     parameters:
     *       - name: id
     *         type: integer
     *         in: path
     *         required: true
     *     responses:
     *       200:
     *         $ref: '#/components/responses/GroupGetByIdResponse'
     */

    app.get('/group/:id', isAuth(), isRoot, validate([
        param('id').isInt().bail()
            .custom(id => table.group.isExisting({ id })),
    ]), (req, res, next) =>
        /**
         * @openapi
         * components:
         *   responses:
         *     GroupGetByIdResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             $ref: '#/components/schemas/GroupSchema'
         */
        table.group.get().then(group => {
            const {
                id,
                name,
            } = group[0];
            res.json({ id, name });
        }).catch(next)
    );

    /**
     * @openapi
     * /group:
     *   post:
     *     tags:
     *       - group
     *     security:
     *       - token: []
     *     summary: create a group
     *     requestBody:
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/GroupCreateRequest'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/GroupCreateResponse'
     *
     * components:
     *   schemas:
     *     GroupCreateRequest:
     *       type: object
     *       properties:
     *         name:
     *           type: string
     *       example:
     *         name: test
     */
    app.post('/group', isAuth(), isRoot, validate([
        body('name').isString()
            .custom(name => table.group.isAlreadyFound({ name })),
    ]), (req, res, next) =>
        /**
         * @openapi
         * components:
         *   responses:
         *     GroupCreateResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               id:
         *                 type: integer
         *                 description: group id
         *                 example: 10
         */
        table.group.create({
            name: req.body.name,
        }).then(id => res.json({ id })).catch(next)
    );

    /**
     * @openapi
     * /group/{id}:
     *   put:
     *     tags:
     *       - group
     *     security:
     *       - token: []
     *     summary: update group by id
     *     parameters:
     *       - name: id
     *         in: path
     *         required: true
     *         schema:
     *           type: integer
     *     requestBody:
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/GroupUpdateRequest'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseMessage'
     *
     * components:
     *   schemas:
     *     GroupUpdateRequest:
     *       type: object
     *       properties:
     *         name:
     *           type: string
     */
    app.put('/group/:id', isAuth(), isRoot, validate([
        param('id').isInt().bail()
            .custom(id => table.group.isExisting({ id })),
        body('name').isString().bail()
            .custom((name, { req }) => table.group.isAlreadyFound({ name }, { id: req.params.id })),
    ]), (req, res, next) =>
        table.group.update(
            { id: req.params.id },
            { name: req.body.name }
        ).then(() => res.json({ msg: 'Group Updated' })).catch(next)
    );

    /**
     * @openapi
     * /group/{id}:
     *   delete:
     *     tags:
     *       - group
     *     security:
     *       - token: []
     *     summary: delate group by id
     *     description: Delete group, You can't delete in use group
     *     parameters:
     *       - name: id
     *         in: path
     *         required: true
     *         schema:
     *           type: integer
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseMessage'
     */

    app.delete('/group/:id', isAuth(), isRoot, validate([
        param('id').isInt().bail()
            .custom(id => table.group.isExisting({ id })),
    ]), (req, res, next) => {
        table.group.delete({ id: req.params.id }).then(() =>
            res.json({ msg: 'Group Deleted' })
        ).catch(next);
    });
};
/**
 * @openapi
 * components:
 *   schemas:
 *     GroupSchema:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *         name:
 *           type: string
 */
