import { param, query, body } from 'express-validator';
import { RouteFunction } from '../@types/routes';
import { createHash } from 'crypto';
import argon2 from 'argon2';
import * as jwt from '../models/jwt';
import { db } from '../db/mysql';
import { isAuth } from '../middleware';
import { ClientError, UserError } from '../models/errorException';
import { permissionCheck } from '../services/role';
import { isSameGroup } from '../services/group';
import { sendMail } from '../models/mail';
import * as Api from '../../api';

export default ((app, validate) => {
    /**
     * @openapi
     * /user:
     *    post:
     *      tags:
     *        - user
     *      security:
     *        - token: []
     *      summary: create an user
     *      description: Create user
     *      requestBody:
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/UserCreateRequest'
     *      responses:
     *        200:
     *          $ref: '#/components/responses/UserCreateResponse'
     *        409:
     *          description: Email Already Existing
     *
     * components:
     *   schemas:
     *     UserCreateRequest:
     *       type: object
     *       properties:
     *         name:
     *           type: string
     *         roleId:
     *           type: integer
     *         groupId:
     *           type: integer
     *         email:
     *           type: string
     *         password:
     *           type: string
     *       example:
     *         name: test
     *         roleId: 1
     *         groupId: 1
     *         email: test@test.com.tw
     *         password: test
     */
    app.post<
    never,
    Api.PostUserCreateResponse,
    Api.PostUserCreateRequestBody>('/user', isAuth('user.create'), validate([
        body('name').isString(),
        body('groupId').isInt().bail()
            .custom((id, { req }) => req.isRoot || isSameGroup(req.idGroup, id)).bail()
            .custom(id => db('group').isExisting({ id })),
        body('roleId').isInt().bail()
            .custom((id, { req }) => db('role').isExisting({ id, groupId: req.body.groupId })).bail()
            .custom((id, { req }) => permissionCheck(req.idRole, id)),
        body('email').isEmail().bail().custom(email => db('user').isAlreadyFound({ email })),
        body('password').isString(),
    ]), (req, res, next) => {
        /**
         * @openapi
         * components:
         *   responses:
         *     UserCreateResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               id:
         *                 type: integer
         *                 description: user id
         *                 example: 10
         */
        const {
            name,
            roleId,
            groupId,
            email,
            password,
        } = req.body;
        argon2.hash(password).then(hash =>
            db('user').insert({ name, roleId, groupId, email, password: hash }).then(result =>
                res.json({ id: result[0] })
            )
        ).catch(next);
    });
    /**
     * @openapi
     * /login:
     *   post:
     *     tags:
     *       - user
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/UserLoginRequest'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/UserLoginResponse'
     *       401:
     *         description: Wrong Password
     *       404:
     *         description: User Not Found
     *
     * components:
     *   schemas:
     *     UserLoginRequest:
     *       type: object
     *       properties:
     *         email:
     *           type: string
     *         password:
     *           type: string
     *         rememberMe:
     *           type: boolean
     *       example:
     *         email: test@test.com.tw
     *         password: test
     *         rememberMe: 0
     */
    app.post<never, Api.PostUserLoginResponse, Api.PostUserLoginRequestBody>('/login', validate([
        body('email').isEmail().bail().custom(email => db('user').isExisting({ email })),
        body('password').isString().bail()
            .custom((password, { req }) =>
                db('user').where({ email: req.body.email }).then(rows =>
                    argon2.verify(rows[0].password, password).then(isPasswd => {
                        if (!isPasswd) {
                            throw new ClientError(401);
                        }
                    })
                )
            ),
        body('rememberMe').optional().isBoolean(),
    ]), (req, res, next) => {
        /**
         * @openapi
         * components:
         *   responses:
         *     UserLoginResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               id:
         *                 type: integer
         *               token:
         *                 type: string
         *                 example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXsaJ9.eyJlbWFpbCI6ImludEBicmlsbHRlay5jb20udHciLCJ1c2VyX2lkIjoxLCJpYXQiOjE2NjQ4NTkzNjMsImV4cCI6MTY2NzQ1MTM2M30.CFrQB_7MXgmkNtto_DudwXj-3hp6z5HM6-rdMNnSyWs
         */
        const email = req.body.email;
        const expiry = req.body.rememberMe ? '1y' : undefined;
        db('user').where({ email }).then(rows =>
            jwt.sign({ email, userId: rows[0].id }, expiry).then(token =>
                res.json({ id: rows[0]?.id, token })
            )
        ).catch(next);
    });
    /**
     * @openapi
     * /user/reset:
     *   post:
     *     tags:
     *       - user
     *     summary: require reset user password
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/UserResetRequest'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseMessage'
     *
     * components:
     *   schemas:
     *     UserResetRequest:
     *       type: object
     *       properties:
     *         email:
     *           type: string
     *         host:
     *           type: string
     */
    app.post<never, Api.PostUserResetResponse, Api.PostUserResetRequestBody>('/user/reset', validate([
        body('email').isEmail().bail()
            .custom(email => db('user').isExisting({ email })),
        body('host').isURL(),
    ]), (req, res, next) => {
        const token2fa = Math.floor(100000 + Math.random() * 900000);
        const token = createHash('sha512').update(String(token2fa)).digest('hex');

        sendMail({
            email: req.body.email,
            subject: res.__('ResetPassword.Subject'),
            contentText: `${res.__('ResetPassword.Sentence1')},\n${res.__('ResetPassword.ConfirmCode')}: ${token2fa}\n${res.__('ResetPassword.Link')}: ${req.body.host}/reset-password/${token}`,
            contentHtml: `<img style="filter: invert(100%)" src="https://imf-go.com/images/logo/logo.svg" height="30"> - <h1 style="display: inline; vertical-align: middle">IMF-GO</h1><br/ ><br /><p>${res.__('ResetPassword.Sentence1')}</p><p>${res.__('ResetPassword.ConfirmCode')}: ${token2fa}</p><br /><p>${res.__('ResetPassword.Link')}: </p><a href="${req.body.host}/reset-password/${token}"><button>${res.__('ResetPassword.Button')}</button>`,
        });

        db('user').update({ resetToken: token }).where({ email: req.body.email }).then(() =>
            res.json({ msg: 'Mail Sent' })
        ).catch(next);
    });
    /**
     * @openapi
     * /user/reset/{token}:
     *   post:
     *     tags:
     *       - user
     *     summary: reset user password by token
     *     parameters:
     *       - name: token
     *         in: path
     *         required: true
     *         schema:
     *           type: string
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/UserResetPasswordByTokenRequest'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseMessage'
     *
     * components:
     *   schemas:
     *     UserResetPasswordByTokenRequest:
     *       type: object
     *       properties:
     *         email:
     *           type: string
     *         password:
     *           type: string
     */
    app.post<
    Api.PostUserResetTokenRequestParams,
    Api.PostUserResetTokenResponse,
    Api.PostUserResetTokenRequestBody>('/user/reset/:token', validate([
        body('email').isEmail().bail()
            .custom(email => db('user').isExisting({ email })),
        param('token').isString().bail()
            .custom((token, { req }) =>
                db('user').where({ email: req.body.email, resetToken: token }).then(user => {
                    if (user.length === 0) {
                        return Promise.reject(new ClientError(401));
                    }
                })
            ),
        body('password').isString(),
    ]), (req, res, next) => {
        argon2.hash(req.body.password).then(hash =>
            db('user')
                .update({ password: hash, resetToken: null })
                .where({ email: req.body.email, resetToken: req.params.token })
                .then(() =>
                    res.json({ msg: 'User Reset' })
                )
        ).catch(next);
    });
    /**
     * @openapi
     * /user/reset/verify/{token}:
     *   get:
     *     tags:
     *       - user
     *     summary: verify reset token
     *     parameters:
     *       - name: token
     *         in: path
     *         required: true
     *         schema:
     *           type: string
     *       - name: email
     *         in: query
     *         required: true
     *         schema:
     *           type: string
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseMessage'
     *       401:
     *         $ref: '#/components/responses/BaseResponseMessage'
     */
    app.get<
    Api.GetUserResetTokenVerifyRequestParams,
    Api.GetUserResetTokenVerifyResponse,
    never,
    Api.GetUserResetTokenVerifyRequestQuery>('/user/reset/verify/:token', validate([
        query('email').isEmail().bail()
            .custom(email => db('user').isExisting({ email })),
        param('token').isString(),
    ]), (req, res, next) => {
        db('user').where({ email: req.query.email, resetToken: req.params.token }).then(user => {
            if (user.length > 0) {
                res.json({ msg: 'Verify Success' });
            } else {
                res.status(401).json({ msg: 'Verify Failed' });
            }
        }).catch(next);
    });
    /**
     * @openapi
     * /users:
     *   get:
     *     tags:
     *       - user
     *     security:
     *       - token: []
     *     summary: get users
     *     responses:
     *       200:
     *         $ref: '#/components/responses/UserListResponse'
     */
    app.get<never, Api.GetUsersResponse>('/users', isAuth('user.get'), validate([]), (req, res, next) =>
        /**
         * @openapi
         * components:
         *   responses:
         *     UserListResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             type: array
         *             items:
         *               $ref: '#/components/schemas/UserSchema'
         */
        db('user').then(rows => rows.map(row => {
            const {
                id,
                name,
                roleId,
                groupId,
                email,
            } = row;
            return { id, name, roleId, groupId, email };
        })).then(rows => res.json(rows)).catch(next)
    );
    /**
     * @openapi
     * /user/{id}:
     *   get:
     *     tags:
     *       - user
     *     security:
     *       - token: []
     *     summary: get user by id
     *     parameters:
     *       - name: id
     *         in: path
     *         required: true
     *         schema:
     *           type: integer
     *     responses:
     *       200:
     *         $ref: '#/components/responses/UserGetSelfResponse'
     */
    app.get<Api.GetUserRequestParams, Api.GetUserResponse>('/user/:id', isAuth('user.get'), validate([
        param('id').isInt().bail()
            .custom((id, { req }) => db('user').isExisting({ id, groupId: req.idGroup })),
    ]), (req, res, next) => {
        /**
         * @openapi
         * components:
         *   responses:
         *     UserGetSelfResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             $ref: '#/components/schemas/UserSchema'
         */
        db('user').where({ id: req.params.id }).first().then(user => {
            if (!user) throw new UserError('User');
            const {
                id,
                name,
                roleId,
                groupId,
                email,
            } = user;
            res.json({ id, name, roleId, groupId, email });
        }).catch(next);
    });
    /**
     * @openapi
     * /user/self/permission:
     *   get:
     *     tags:
     *       - user
     *     security:
     *       - token: []
     *     summary: get self permission
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseArrayString'
     */
    app.get('/user/self/permission', isAuth(), validate([]), (req, res, next) =>
        db('role').where({ id: req.idRole }).first().then(role =>
            res.json(JSON.parse(role?.permissions || ''))
        ).catch(next)
    );
    /**
     * @openapi
     * /user/{id}:
     *   put:
     *     tags:
     *       - user
     *     security:
     *       - token: []
     *     summary: update user by id
     *     parameters:
     *       - name: id
     *         in: path
     *         required: true
     *         schema:
     *           type: integer
     *     requestBody:
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/UserUpdateByIDRequest'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseMessage'
     *       409:
     *         description: Email Already Existing
     *
     * components:
     *   schemas:
     *     UserUpdateByIDRequest:
     *       type: object
     *       properties:
     *         roleId:
     *           type: integer
     *         groupId:
     *           type: integer
     *         name:
     *           type: string
     *         email:
     *           type: string
     *         password:
     *           type: string
     *       example:
     *         roleId: 1
     *         groupId: 1
     *         name: test
     *         email: test@test.com.tw
     *         password: string
     */
    app.put<
    Api.PutUserRequestParams,
    Api.PutUserResponse,
    Api.PutUserRequestBody>('/user/:id', isAuth('user.update'), validate([
        param('id').isInt().bail()
            .custom((id, { req }) => db('user').isExisting({ id, groupId: req.idGroup })),
        body('roleId').isInt().bail()
            .custom(id => db('role').isExisting({ id })).bail()
            .custom((id, { req }) => permissionCheck(req.idRole, id)),
        body('groupId').isInt()
            .custom(id => db('group').isExisting({ id })).bail()
            .custom((id, { req }) => isSameGroup(req.idGroup, id)),
        body('name').isString(),
        body('email').isEmail().bail()
            .custom((email, { req }) => db('user').isAlreadyFound({ email }, { id: req.params?.id })),
        body('password').optional().isString(),
    ]), (req, res, next) => {
        const {
            name,
            email,
            password,
        } = req.body;
        const promise = [];

        promise.push(
            db('user').update({ name, email }).where({ id: req.params.id })
        );

        if (password !== undefined) {
            promise.push(
                argon2.hash(password).then(hash => db('user').update({ password: hash }).where({ id: req.params.id }))
            );
        }

        Promise.all(promise).then(() =>
            res.json({ msg: 'User Updated' })
        ).catch(next);
    });
    /**
     * @openapi
     * /user/{id}:
     *   delete:
     *     tags:
     *       - user
     *     security:
     *       - token: []
     *     summary: delate user by id
     *     description: Delete user, You can't self delete
     *     parameters:
     *       - name: id
     *         in: path
     *         required: true
     *         schema:
     *           type: integer
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseMessage'
     */
    app.delete<
    Api.DeleteUserRequestParams,
    Api.DeleteUserResponse>('/user/:id', isAuth('user.delete'), validate([
        param('id').isInt().bail()
            .custom((id, { req }) => db('user').isExisting({ id, groupId: req.idGroup })).bail()
            // can't delete self
            .custom((id, { req }) => req.idUser === id ? Promise.reject(new ClientError(403)) : Promise.resolve()),
    ]), (req, res, next) => {
        db('user').del().where({ id: req.params.id }).then(() =>
            res.json({ msg: 'User Deleted' })
        ).catch(next);
    });
}) as RouteFunction;
/**
 * @openapi
 * components:
 *   schemas:
 *     UserSchema:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *         name:
 *           type: string
 *         roleId:
 *           type: integer
 *         groupId:
 *           type: integer
 *         email:
 *           type: string
 */
