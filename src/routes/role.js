const { param, body } = require('express-validator');
const { table } = require('../db/mysql');
const { isAuth } = require('../middleware');
const { ClientError } = require('../models/errorException');

module.exports = (app, validate) => {
    /**
     * @openapi
     * /roles:
     *   get:
     *     tags:
     *       - role
     *     security:
     *       - token: []
     *     summary: get all roles
     *     responses:
     *       200:
     *         $ref: '#/components/responses/RoleGetResponse'
     */

    app.get('/roles', isAuth('role.get'), validate([]), (req, res, next) =>
        /**
         * @openapi
         * components:
         *   responses:
         *     RoleGetResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             type: array
         *             items:
         *               $ref: '#/components/schemas/RoleSchema'
         */
        table.role.get({ groupId: req.idGroup }).then(rows => rows.map(row => {
            const {
                id,
                name,
                permissions,
            } = row;
            return { id, name, permissions: JSON.parse(permissions) };
        })).then(rows => res.json(rows)).catch(next)
    );

    /**
     * @openapi
     * /role/{id}:
     *   get:
     *     tags:
     *       - role
     *     security:
     *       - token: []
     *     summary: get role by id
     *     parameters:
     *       - name: id
     *         type: integer
     *         in: path
     *         required: true
     *     responses:
     *       200:
     *         $ref: '#/components/responses/RoleGetByIdResponse'
     */

    app.get('/role/:id', isAuth('role.get'), validate([
        param('id').isInt().bail()
            .custom(id => table.role.isExisting({ id })),
    ]), (req, res, next) =>
        /**
         * @openapi
         * components:
         *   responses:
         *     RoleGetByIdResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             $ref: '#/components/schemas/RoleSchema'
         */
        table.role.get({ id: req.params.id, groupId: req.idGroup }).then(role => {
            const {
                id,
                name,
                permissions,
            } = role[0];
            res.json({ id, name, permissions: JSON.parse(permissions) });
        }).catch(next)
    );

    /**
     * @openapi
     * /role:
     *   post:
     *     tags:
     *       - role
     *     security:
     *       - token: []
     *     summary: create a role
     *     requestBody:
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/RoleCreateRequest'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/RoleCreateResponse'
     *
     * components:
     *   schemas:
     *     RoleCreateRequest:
     *       type: object
     *       properties:
     *         name:
     *           type: string
     *         permissions:
     *           type: array
     *           items:
     *             type: string
     */
    app.post('/role', isAuth('role.create'), validate([
        body('name').isString().bail()
            .custom(name => table.role.isAlreadyFound({ name })),
        body('permissions').isArray().bail(),
        // TODO: body('permissions.*').check(permission => Permission.isExisting())
        // TODO: permission check
    ]), (req, res, next) =>
        /**
         * @openapi
         * components:
         *   responses:
         *     RoleCreateResponse:
         *       description: Success
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               id:
         *                 type: integer
         *                 description: role id
         *                 example: 10
         */
        table.role.create({
            groupId: req.idGroup,
            name: req.body.name,
            permissions: JSON.stringify(req.body.permissions),
        }).then(id => res.json({ id })).catch(next)
    );

    /**
     * @openapi
     * /role/{id}:
     *   put:
     *     tags:
     *       - role
     *     security:
     *       - token: []
     *     summary: update role by id
     *     parameters:
     *       - name: id
     *         in: path
     *         required: true
     *         schema:
     *           type: integer
     *     requestBody:
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/RoleUpdateRequest'
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseMessage'
     *
     * components:
     *   schemas:
     *     RoleUpdateRequest:
     *       type: object
     *       properties:
     *         name:
     *           type: string
     *         permissions:
     *           type: array
     *           items:
     *             type: string
     */
    app.put('/role/:id', isAuth('role.update'), validate([
        param('id').isInt().bail()
            .custom((id, { req }) => table.role.isExisting({ id, groupId: req.idGroup })),
        body('name').isString().bail()
            .custom((name, { req }) => table.role.isAlreadyFound({ name }, { id: req.params.id })),
        body('permissions').isArray().bail(),
        // TODO: body('permissions.*').check(permission => Permission.isExisting())
        // TODO: permission check
    ]), (req, res, next) =>
        table.role.update(
            { id: req.params.id, groupId: req.idGroup },
            { name: req.body.name, permissions: JSON.stringify(req.body.permissions) }
        ).then(() => res.json({ msg: 'Role Updated' })).catch(next)
    );

    /**
     * @openapi
     * /role/{id}:
     *   delete:
     *     tags:
     *       - role
     *     security:
     *       - token: []
     *     summary: delate role by id
     *     description: Delete role, You can't delete in use role
     *     parameters:
     *       - name: id
     *         in: path
     *         required: true
     *         schema:
     *           type: integer
     *     responses:
     *       200:
     *         $ref: '#/components/responses/BaseResponseMessage'
     */

    app.delete('/role/:id', isAuth('role.delete'), validate([
        param('id').isInt().bail()
            .custom((id, { req }) => table.role.isExisting({ id, groupId: req.idGroup })).bail()
            // can't delete in use role
            .custom(id => table.user.get({ roleId: id }).then(rows =>
                rows.length > 0 ? Promise.reject(new ClientError(403, 'Role in used')) : Promise.resolve()
            )),
    ]), (req, res, next) => {
        table.role.delete({ id: req.params.id, groupId: req.idGroup }).then(() =>
            res.json({ msg: 'Role Deleted' })
        ).catch(next);
    });
};
/**
 * @openapi
 * components:
 *   schemas:
 *     RoleSchema:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *         name:
 *           type: string
 *         permissions:
 *           type: array
 *           items:
 *             type: string
 */
