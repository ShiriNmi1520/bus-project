import { db } from './mysql';
import * as mysqlFunction from './mysql/function';

export const mysql = db;

export const functions = {
    mysql: mysqlFunction,
};
