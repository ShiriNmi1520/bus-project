import logger from '../../models/log';
import { db } from './index';

export async function migrate() {
    const directory = `${process.cwd()}/seeds/`;

    await db.migrate.latest();
    logger.info('Knex Migrate Latest done');
    await Promise.all([
        db('user').where({ id: 1 }).then(rows => {
            if (rows.length === 0) {
                return db.seed.run({ directory: directory + '/newSystem/' });
            }
        }),
        db.seed.run({ directory, specific: 'permission.js' }),
    ]);
}
