import config from '../../config';
import knex from 'knex';
import { ClientError, DatabaseError } from '../../models/errorException';

export const db = knex({
    client: 'mysql',
    connection: {
        host: config.mysql.host,
        port: config.mysql.port,
        user: config.mysql.user,
        password: config.mysql.password,
        database: config.mysql.database,
    },
});

knex.QueryBuilder.extend('isExisting', function(info) {
    return this.where(info).catch(error => Promise.reject(new DatabaseError(error))).then(rows => {
        if (!rows || !Array.isArray(rows)) return new DatabaseError('Unknown Result');
        if (rows.length === 0) return Promise.reject(new ClientError(404));
        return rows;
    });
});

knex.QueryBuilder.extend('isAlreadyFound', function(info) {
    return this.where(info).catch(error => Promise.reject(new DatabaseError(error))).then(rows => {
        if (!rows || !Array.isArray(rows)) return new DatabaseError('Unknown Result');
        if (rows.length > 0) return Promise.reject(new ClientError(409));
        return false;
    });
});

// test custom query builder
// db('user').isExisting({ id: 0 }).then(logger.debug).catch(logger.error);
// db('user').isAlreadyFound({ id: 1 }).then(logger.debug).catch(logger.error);
