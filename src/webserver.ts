import express from 'express';
import * as Sentry from '@sentry/node';
import * as Tracing from '@sentry/tracing';
import { RewriteFrames } from '@sentry/integrations';
import { I18n } from 'i18n';
import path from 'path';
import ExpressValidator, { validationResult } from 'express-validator';
import BodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';
import config from './config';
import { logger } from './models/log';
import { decodeSync } from './models/jwt';
import { Validate } from './@types/webserver';

const WebServer = express();

// SENTRY
const version = 'VERSION_TO_REPLACE';
Sentry.init({
    dsn: config.sentry.dsn,
    release: version,
    integrations: [
    // enable HTTP calls tracing
        new Sentry.Integrations.Http({ tracing: true }),
        // enable Express.js middleware tracing
        new Tracing.Integrations.Express({ app: WebServer }),
        new RewriteFrames({
            root: global.__dirname,
        }),
    ],
    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
    environment: config.app.env,
});
// RequestHandler creates a separate execution context using domains, so that every
// transaction/span/breadcrumb is attached to its own Hub instance
WebServer.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
WebServer.use(Sentry.Handlers.tracingHandler());

// MORGAN LOG
if (config.app.env !== 'test') {
    morgan.token('user', req => {
        if (req.headers.authorization) {
            try {
                return decodeSync(req.headers.authorization).toString();
            } catch (error) {
                return 'error';
            }
        }
        return undefined;
    });

    WebServer.use(
        morgan((tokens, req, res) => {
            return JSON.stringify({
                'remote-address': tokens['remote-user']?.(req, res),
                'time': tokens.date?.(req, res, 'iso'),
                'method': tokens['method']?.(req, res),
                'url': tokens['url']?.(req, res),
                //'http-version': tokens['http-version'](req, res),
                'status-code': tokens['status']?.(req, res),
                //'content-length': tokens['res'](req, res, 'content-length'),
                'referrer': tokens['referrer']?.(req, res),
                'user-agent': tokens['user-agent']?.(req, res),
                'response-time': tokens['response-time']?.(req, res),
                'user': tokens['user']?.(req, res),
            });
        })
    );
}

// I18N
const i18n = new I18n({
    locales: ['en', 'zh'],
    defaultLocale: 'en',
    directory: path.join(__dirname, '/locales'),
});
WebServer.use(i18n.init);

WebServer.use(cors());
WebServer.use(BodyParser.json({ type: 'application/json' }));
WebServer.use(BodyParser.urlencoded({ extended: false }));

if (config.app.env === 'master') {
    WebServer.use(helmet());
}

WebServer.get('/health', (req, res) => res.json({ msg: 'I\'m alive' }));
WebServer.get('/', (req, res) => res.send('Template API'));

import { responseMessage } from './models/errorException';

// SEQUENTIAL PROCESSING, stops running validations chain if the previous one have failed.
const validate: Validate = validations => async (req, res, next) => {
    let statusCode = 400;
    for (const validation of validations) {
        const result: ExpressValidator.Result = await validation.run(req);
        result.array().map(error => {
            if (typeof error.msg !== 'object') return error;
            if (error.msg.statusCode === undefined || error.msg.message === undefined) {
                return error;
            }
            statusCode = error.msg.statusCode;
            error.msg = error.msg.message;
            return error;
        });
        if (result.array().length) break;
    }

    const errors = validationResult(req);
    if (errors.isEmpty()) {
        next();
    } else {
        res.status(statusCode).json({ errors: errors.array() });
        next('route');
    }
};

// REQUEST TIMEOUT
WebServer.use((req, res, next) => {
    res.setTimeout(config.app.timeout, function() {
        logger.error('Request has timed out.');
        res.status(408).send(responseMessage[408]);
    });
    next();
});

// SYSTEM LOG
import('./middleware/systemLog').then(module => WebServer.use(module.default));

// ADD ROUTES
import('./routes/user').then(module => module.default(WebServer, validate));
import('./routes/permission').then(module => module.default(WebServer, validate));
import('./routes/subscribe').then(module => module.default(WebServer, validate));
// require('./routes/user')(WebServer, validate);
// require('./routes/permission')(WebServer, validate);
// require('./routes/role')(WebServer, validate);
// require('./routes/group')(WebServer, validate);

// The error handler must be before any other error middleware and after all controllers
if (config.app.env !== 'test') {
    // The error handler must be before any other error middleware and after all controllers
    WebServer.use(Sentry.Handlers.errorHandler() as express.ErrorRequestHandler);
}

// Optional fallthrough error handler
WebServer.use(((err, req, res, next) => {
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    if (err instanceof Error) {
        logger.error(err.stack);
    } else {
        logger.error(err);
    }
    // res.statusCode = 500;
    // res.end(res.sentry + '\n');
    res.status(500).send(responseMessage[500]);
    next(err);
}) as express.ErrorRequestHandler);

// SWAGGER_BEGIN
import swagger from './models/swagger';
if (config.app.env === 'development' || config.app.env === 'develop') {
    swagger(WebServer);
}
// SWAGGER_END

export default WebServer;

/**
 * @openapi
 * components:
 *   schemas:
 *     ArrayString:
 *       type: array
 *       items:
 *         type: string
 *       example: [string]
 *   responses:
 *     BaseResponseMessage:
 *       description: Success
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               msg:
 *                 type: string
 *     BaseResponseArrayString:
 *       description: Success
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ArrayString'
 */
