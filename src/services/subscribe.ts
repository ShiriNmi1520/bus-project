import {SubscribeError, MiddlewareError} from '../models/errorException';
import { db } from '../db/mysql';
import logger from '../models/log';

export const isAlreadySubscribed = (email: string | null, clientID: string | null, busNo: string, station: string) =>
  new Promise((resolve, reject) => {
    db('subscribe').where({ email, clientID, busNo, station}).whereIn('status', ['unsubscribed']).then(data => {
      if (data) {
        reject(new SubscribeError('Already subscribed'));
        return;
      }
      resolve(true)
    })
      .catch(sqlError => {
        if (sqlError instanceof Error) throw new MiddlewareError(sqlError.message);
        logger.error(sqlError);
        throw new MiddlewareError(sqlError as string);
      })
  })
