import { db } from '../db/mysql';
import { ClientError, RoleError } from '../models/errorException';

export const permissionCheck = async (userCurrentRoleId: number, toOperationRoleId: number) =>
    new Promise((resolve, reject) => {
        Promise.all([
            db('role').where({ id: userCurrentRoleId }).first('permissions'),
            db('role').where({ id: toOperationRoleId }).first('permissions'),
        ]).then(results => {
            if (!results[0] || !results[1]) {
                reject(new RoleError('Role Not Found'));
                return;
            }
            const userPermissions: Array<string> = JSON.parse(results[0].permissions);
            const toOperationPermissions: Array<string> = JSON.parse(results[1].permissions);
            // User have all permission
            if (userPermissions.indexOf('*') !== -1) resolve(true);
            // User haven't permission
            if (userPermissions.length === 0) reject(new ClientError(403));
            // To Operation User haven't permission
            if (toOperationPermissions.length === 0) resolve(true);

            const hasPermission = toOperationPermissions.every(toOperationPermission =>
                userPermissions.some(permission => {
                    if (permission === '*') return true;
                    const re = new RegExp('^' + permission);
                    return re.test(toOperationPermission);
                })
            );
            if (!hasPermission) reject(new ClientError(403));
            resolve(true);
        });
    });
