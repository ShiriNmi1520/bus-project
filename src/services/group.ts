import { ClientError } from '../models/errorException';

export const isSameGroup = (userGroupId:number, toOperationGroupId: number) => new Promise((resolve, reject) => {
    if (userGroupId !== toOperationGroupId) {
        reject(new ClientError(403));
    } else {
        resolve(true);
    }
});
