import { permissions } from '../config/permission';
import { db } from '../db/mysql';
import { MiddlewareError } from '../models/errorException';
import logger from '../models/log';

export const hasPermission = async (permissionName: string, roleId: number) => {
    // Check permission name in seed
    if (permissions.indexOf(permissionName) === -1) {
        throw new MiddlewareError(
            `Permission Not Found In Seed, Please Add '${permissionName}' To seeds/permission.js`
        );
    }
    // Check permission name in permission table
    await db('permission').isExisting({ name: permissionName }).catch(() => {
        throw new MiddlewareError(
            `Permission Not Found In Table, Please Add '${permissionName}' To Permission Table`
        );
    });
    // Check user has permission
    try {
        const [role] = await db('role').where({ id: roleId });
        if (role) {
            const rolePermissions: Array<string> = JSON.parse(role.permissions);
            return rolePermissions.some(permission => {
                // All permission
                if (permission === '*') return true;
                const re = new RegExp('^' + permission);
                return re.test(permissionName);
            });
        } else {
            throw new Error(`User Role Not Found, ID = ${roleId}`);
        }
    } catch (error) {
        if (error instanceof Error) throw new MiddlewareError(error.message);
        logger.error(error);
        throw new MiddlewareError(error as string);
    }
};
