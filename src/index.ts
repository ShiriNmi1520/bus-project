import app from './webserver';
import { functions } from './db';
import config from './config';
import logger from './models/log';
import mail from './models/mail';

const init = async () => {
  if (config.app.env === 'development') {
    const { ESLint } = await import('eslint');
    const eslint = new ESLint();
    const results = await eslint.lintFiles(['**/*.js', '**/*.ts']);
    const formatter = await eslint.loadFormatter('stylish');
    const resultText = formatter.format(results);
    if (resultText) logger.error(resultText);
  }

  await Promise.all([
    functions.mysql.migrate(),
    mail.init(),
  ]);
  const { port } = config.app;
  app.listen(port);
  logger.info('Template API Server Enable');
  logger.info(`Listen Port:${port}`);

  // const initSocket = require('./socket');
  // const portSocket = config.app.portSocket;
  // const io = initSocket();
  // io.listen(portSocket);
};
init();

process.on('uncaughtException', (err) => {
  logger.error(err);
  logger.info('Node NOT Exiting...');
});
