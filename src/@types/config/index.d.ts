export interface Config {
    debug: boolean | undefined,
    npmScript: string,
    app: {
        port: number,
        portSocket: number,
        env: string,
        timeout: number,
    },
    mail: {
        host: string | undefined,
        user: string | undefined,
        password: string | undefined,
        from: string,
    },
    redis: {
        host: string,
        port: number,
    },
    mysql: {
        host: string,
        port: number,
        user: string,
        password: string,
        database: string,
    },
    jwt: {
        secret: string,
        expiry: string,
    },
    sentry: {
        dsn: string | undefined,
    },
    permissions: Array<string>
}
