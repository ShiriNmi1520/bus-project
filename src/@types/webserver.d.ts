import { RequestHandler } from 'express';
import { ValidationChain } from 'express-validator';

export type Validate = (
    validations: Array<ValidationChain>
) => RequestHandler
