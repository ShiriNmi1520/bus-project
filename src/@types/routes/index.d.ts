import { Express } from 'express';
import { JwtPayload } from 'jsonwebtoken';
import { Validate } from '../webserver';

export interface AuthorizationPayload extends JwtPayload {
    email: string
    userId: number
}

export type RouteFunction = (
    app: Express,
    validate: Validate
) => void;
