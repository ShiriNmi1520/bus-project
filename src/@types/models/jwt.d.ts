import { AuthorizationPayload } from '..';

export type Sign = (
    payload: string | Buffer | object,
    expiry?: string
) => Promise<string>

export type Decode = (
    token: string
) => Promise<string | Buffer | object | AuthorizationPayload>

export type DecodeSync = (
    token: string
) => string | Buffer | object | AuthorizationPayload

export type Verify = (
    token: string
) => Promise<boolean>
