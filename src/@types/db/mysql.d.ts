import * as Table from './tables';
import { Knex as KnexOriginal } from 'knex';

declare module 'knex' {
  namespace Knex {
    interface QueryBuilder {
      isExisting<TRecord, TResult>(info: TRecord): KnexOriginal.QueryBuilder<TRecord, TResult>;
      isAlreadyFound<TRecord, TResult>(info: TRecord, exclude?: TRecord): KnexOriginal.QueryBuilder<TRecord, TResult>;
    }
  }
}

declare module 'knex/types/tables' {
  interface Tables {
    user: KnexOriginal.CompositeTableType<
      // where
      Table.User,
      // insert required
      Pick<Table.User, 'name' | 'roleId' | 'groupId' | 'email' | 'password'> &
      // insert optional
      Partial<Pick<Table.User, 'createdAt' | 'updatedAt'>>,
      // update
      Partial<Omit<Table.User, 'id' | 'root'>>
    >

    permission: KnexOriginal.CompositeTableType<
      // where
      Table.Permission,
      // insert required
      Pick<Table.Permission, 'name'> &
      // insert optional
      Partial<Pick<Table.Permission, 'createdAt'>>,
      // update
      Partial<Omit<Table.Permission, 'id'>>
    >

    group: KnexOriginal.CompositeTableType<
      // where
      Table.Group,
      // insert required
      Pick<Table.Group, 'name'> &
      // insert optional
      Partial<Pick<Table.Group, 'createdAt' | 'updatedAt'>>,
      // update
      Partial<Omit<Table.Group, 'id'>>
    >

    role: KnexOriginal.CompositeTableType<
      // where
      Table.Role,
      // insert required
      Pick<Table.Role, 'name' | 'groupId' | 'permissions'> &
      // insert optional
      Partial<Pick<Table.Role, 'createdAt' | 'updatedAt'>>,
      // update
      Partial<Omit<Table.Role, 'id'>>
    >

    systemLog: KnexOriginal.CompositeTableType<
      // where
      Table.SystemLog,
      // insert required
      // insert optional
      Partial<Table.SystemLog>,
      // update
      never
    >

    subscribe: KnexOriginal.CompositeTableType<
      // where
      Table.Subscribe,
      // insert required
      Pick<Table.Subscribe, 'id' | 'email' | 'clientID' | 'busNo' | 'station' | 'minutesBefore' | 'status'>,
      never,
      // update
      Partial<Omit<Table.Subscribe, 'id'>>
    >
  }
}
