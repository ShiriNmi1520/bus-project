export interface Permission {
    name: string
    createdAt: string
}

export interface Group {
    id: number
    name: string
    createdAt: string
    updatedAt: string
}

export interface Role {
    id: number
    name: string
    groupId: number
    permissions: string
    createdAt: string
    updatedAt: string
}

export interface User {
    id: number
    name: string
    roleId: number
    groupId: number
    email: string
    password: string
    root: 0 | 1
    resetToken: string | null
    createdAt: string
    updatedAt: string
}

export interface SystemLog {
    id: number
    request: string
    responseCode: number
    userId: number
    userRaw: string
    createdAt: string
}

export interface Subscribe {
    id: string
    email: string | null
    clientID: string | null
    busNo: string
    station: string
    minutesBefore: number
    status: 'subscribed' | 'unsubscribed' | 'error'
    createdAt: string
    updatedAt: string
}
