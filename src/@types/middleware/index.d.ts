declare namespace Express {
    interface Request {
        idUser?: number
        idRole?: number
        idGroup?: number
        isRoot?: 0 | 1
    }
}
