export const permissions: Array<string> = [
    'user.get',
    'user.create',
    'user.update',
    'user.delete',
    'user.permission',

    'role.get',
    'role.create',
    'role.update',
    'role.delete',

    'permission.get',
];
