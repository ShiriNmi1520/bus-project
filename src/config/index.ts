import { Config } from '../@types/config';
import { permissions } from './permission';
import { env } from 'process';

const config: Config = {
    debug: env.DEBUG === 'true' || false,
    npmScript: env.npm_lifecycle_event || 'dev',
    app: {
        port: parseInt(env.PORT_REST || '8000'), // Listening Port
        portSocket: parseInt(env.PORT_SOCKET || '1337'), // Socket port
        env: env.NODE_ENV || 'development', // Environment
        timeout: parseInt(env.TIMEOUT || '60000'), // Request Timeout
    },
    mail: {
        host: env.MAIL_HOST || undefined,
        user: env.MAIL_USER || undefined,
        password: env.MAIL_PASSWORD || undefined,
        from: env.MAIL_FROM || 'no-reply@test.com',
    },
    redis: {
        host: env.REDIS_HOST || 'redis',
        port: parseInt(env.REDIS_PORT || '6379'),
    },
    mysql: {
        host: env.MYSQL_HOST || 'mysql',
        port: parseInt(env.MYSQL_PORT || '3306'),
        user: env.MYSQL_USER || 'api',
        password: env.MYSQL_PASSWORD || 'template',
        database: env.MYSQL_DATABASE || 'template',
    },
    jwt: {
        secret: env.TOKEN_SECRET || 'secret',
        expiry: env.TOKEN_EXPIRY || '30d',
    },
    sentry: {
        dsn: env.SENTRY_DSN || undefined,
    },
    permissions,
};

export default config;
