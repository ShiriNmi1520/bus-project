import { createLogger, format, transports } from 'winston';
import config from '../config';
const { combine, simple } = format;

const show = format(info => {
    if (info.level === 'info') {
        console.log(info.message);
        return false;
    } else {
        if (info.message instanceof Object) info.message = JSON.stringify(info.message);
        info.level = info.level.toUpperCase();
        return info;
    }
});


export const logger = createLogger({
    level: 'debug',
    format: combine(show(), format.colorize(), simple()),
    silent: false,
    transports: [
        new transports.Console({
            // colorize: true,
            // prettyPrint: true,
        }),
    ],
});

if (!config.debug) {
    logger.level = 'info';
}

if (config.app.env === 'test') {
    logger.silent = true;
}

export default logger;
