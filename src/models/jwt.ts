import jwt from 'jsonwebtoken';
import config from '../config';
import { Decode, DecodeSync, Sign, Verify } from '../@types/models/jwt';

export const sign: Sign = (payload, expiry = config.jwt.expiry) => new Promise((resolve, reject) => {
    jwt.sign(payload, config.jwt.secret, { expiresIn: expiry }, (error, encoded) => {
        if (encoded !== undefined) {
            resolve(encoded);
        } else {
            reject(error);
        }
    });
});

export const decode: Decode = token => new Promise((resolve, reject) => {
    jwt.verify(token, config.jwt.secret, (error, decoded) => {
        if (decoded !== undefined) {
            resolve(decoded);
        } else {
            reject(error);
        }
    });
});

export const decodeSync: DecodeSync = token => jwt.verify(token, config.jwt.secret);

export const verify: Verify = token => new Promise(resolve => {
    jwt.verify(token, config.jwt.secret, (_, decoded) => {
        if (decoded !== undefined) {
            resolve(true);
        } else {
            resolve(false);
        }
    });
});

