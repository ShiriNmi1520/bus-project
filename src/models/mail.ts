import { createTransport, createTestAccount, TestAccount } from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import config from '../config';
import { logger } from './log';
import { MailError } from './errorException';
let testAccount: TestAccount | undefined = undefined;

const getTransporter = async () => {
    let transporterOptions: SMTPTransport.Options = {};
    if (config.mail.host) {
        transporterOptions = {
            host: config.mail.host,
            port: 587,
            secure: false, // upgrade later with TLS
            auth: {
                user: config.mail.user,
                pass: config.mail.password,
            },
            tls: {
                // do not fail on invalid certs
                rejectUnauthorized: false,
            },
        };
    } else if (testAccount) {
        try {
            transporterOptions = {
                host: testAccount.smtp.host,
                port: testAccount.smtp.port,
                secure: testAccount.smtp.secure,
                auth: {
                    user: testAccount.user,
                    pass: testAccount.pass,
                },
            };
        } catch (error) {
            logger.error(error);
            logger.error('Created test mail transport failed');
        }
    } else {
        logger.error('Created transporter failed, MAIL_HOST not configured');
        return Promise.reject(new MailError());
    }
    const transporter = createTransport(transporterOptions);
    return transporter.verify()
        .then(() => logger.info('Connected to Mail Server'))
        .then(() => transporter)
        .catch(error => {
            logger.error(error);
            return Promise.reject(new MailError());
        });
};

export async function init() {
    if (!config.mail.host && ['development', 'test', 'develop'].indexOf(config.app.env) !== -1) {
        testAccount = await createTestAccount();
        logger.info('Testing Mail account generated on ethereal');
        logger.info('account: ' + testAccount.user);
        logger.info('password: ' + testAccount.pass);
    } else if (!config.mail.host) {
        logger.info('MAIL Function are disabled');
    }
}

type sendMailOptions = {
    subject: string,
    email: string,
    contentText: string,
    contentHtml: string,
}

export function sendMail({ subject, email, contentText, contentHtml }: sendMailOptions) {
    return new Promise((resolve, reject) => {
        const mailOptions = {
            from: config.mail.from,
            to: email,
            subject: subject,
            text: contentText,
            html: contentHtml,
        };
        getTransporter()
            .then(transporter => transporter.sendMail(mailOptions).finally(() => transporter.close()))
            .then(resolve)
            .catch(reject);
    });
}

export default {
    init,
    sendMail,
};
