import { Express } from 'express';
import config from '../config';
import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc, { SwaggerDefinition } from 'swagger-jsdoc';

export const swaggerDefinition: SwaggerDefinition = {
    openapi: '3.0.4',
    info: {
        title: 'Template API',
        version: '1.0.0',
        description: 'Docs to use Template API',
    },
    basePath: '/',
    components: {
        securitySchemes: {
            token: {
                description: 'JWT authorization of an API',
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
            },
        },
    },
};

const init = (app: Express) => {
    const options = {
        swaggerDefinition,
        apis: ['webserver.js', 'routes/**/*.ts'],
    };

    if (['dev', 'test'].some(script => config.npmScript.startsWith(script))) {
        const length = options.apis.length;
        for (let i = 0; i <= length; i += 1) {
            options.apis[i] = 'src/' + options.apis[i];
        }
    }

    const swaggerSpec = swaggerJSDoc(options);

    app.get('/swagger.json', function(req, res) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(swaggerSpec, null, 4));
    });
    const swaggerUiOptions = {
        explorer: true,
        swaggerOptions: {
            docExpansion: 'none',
            url: '/swagger.json',
        },
    };
    if (config.app.env === 'development') {
        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(undefined, swaggerUiOptions));
    }
};

export default init;
