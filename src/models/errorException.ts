interface ResponseMessage {
    400: string
    401: string
    403: string
    404: string
    408: string
    409: string
    500: string
}

export const responseMessage: ResponseMessage = {
    400: 'Bad Request',
    401: 'Unauthorized',
    403: 'Forbidden',
    404: 'Not Found',
    408: 'Request Timeout',
    409: 'Conflict',
    500: 'Internal Server Error',
};

class ErrorWithStatus {
    name: string;
    message: string;
    statusCode: number;
    constructor(
        statusCode: keyof ResponseMessage,
        message?: string
    ) {
        Error.captureStackTrace(this, this.constructor);
        this.name = this.constructor.name;
        this.message = message || responseMessage[statusCode];
        this.statusCode = statusCode;
    }
}
export class ClientError extends ErrorWithStatus { }
export class ServerError extends ErrorWithStatus {
    constructor(message = responseMessage[500]) {
        super(500, message);
    }
}

class ServiceError extends ServerError { }
export class MiddlewareError extends ServiceError { }
export class RoleError extends ServiceError { }
export class GroupError extends ServiceError { }
export class UserError extends ServiceError { }
export class DatabaseError extends ServiceError { }
export class MailError extends ServiceError { }
export class SubscribeError extends ServiceError { }
