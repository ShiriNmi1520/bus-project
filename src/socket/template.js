const { logger } = require('../models/log');

const eventNameExample = 'test';

const success = ':success';
const err = ':err';

module.exports = function(client) {
    client.on(eventNameExample, receiveData => {
        logger.info(receiveData);
        // DOING THINGS
        const response = 'test';
        client.emit(eventNameExample + success, response);
    });

    client.on('error', error => {
        logger.error(error);
        client.emit(eventNameExample + err, JSON.stringify(error));
    });
};
