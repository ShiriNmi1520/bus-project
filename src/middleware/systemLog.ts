import { db } from '../db/mysql';
import { logger } from '../models/log';
import { NextFunction, Request, RequestHandler, Response } from 'express';
import { decodeSync } from '../models/jwt';
import { AuthorizationPayload } from '../@types/routes';

const systemLog: RequestHandler = (req: Request, res: Response, next: NextFunction) => {
    const originalSend = res.send;
    // only catch POST PUT PATCH DELETE
    const catchMethods = ['POST', 'PUT', 'PATCH', 'DELETE'];
    if (catchMethods.indexOf(req.method) > -1) {
        let userRaw: Partial<AuthorizationPayload> | string = {};
        if (req.headers.authorization) {
            try {
                userRaw = decodeSync(req.headers.authorization);
            } catch (_) {
                userRaw = {};
            }
        }

        res.send = function(body) { // res.send() 和 res.json() 都會攔截到
            try {
                db('systemLog').insert({
                    request: JSON.stringify({
                        req: {
                            ...req.body,
                            ...req.params,
                            ...req.query,
                        },
                        res: JSON.parse(body),
                    }),
                    responseCode: res.statusCode,
                    userId: typeof userRaw === 'string' ? undefined : userRaw.userId,
                    userRaw: typeof userRaw === 'string' ? userRaw : JSON.stringify(userRaw),
                });
            } catch (error) {
                logger.error('SystemLogError: ' + error);
            }
            return originalSend.call(this, body);
        };
    }
    next();
};

export default systemLog;
