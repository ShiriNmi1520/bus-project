import { db } from '../db/mysql';
import { logger } from '../models/log';
import { hasPermission } from '../services/permission';
import * as jwt from '../models/jwt';
import { responseMessage } from '../models/errorException';
import { AuthorizationPayload } from '../@types/routes/index';
import { RequestHandler } from 'express';

export const isAuth = (permissionName?: string): RequestHandler =>
    async (req, res, next) => {
        if (!await jwt.verify(req.headers.authorization || '')) return res.status(401).send(responseMessage[401]);
        const decoded = (await jwt.decode(req.headers.authorization || '')) as AuthorizationPayload;
        const [user] = await db('user').where({ id: decoded.userId });

        // Set user to req
        if (!user) {
            logger.error('User Not Found');
            res.status(404).send(responseMessage[404]);
            return;
        }

        // Check permission
        if (!user.root && permissionName && !await hasPermission(permissionName, user.roleId)) {
            res.status(403).send(responseMessage[403]);
            return;
        }

        req.idUser = user.id;
        req.idRole = user.roleId;
        req.idGroup = user.groupId;
        req.isRoot = user.root;
        next();
    };

export const isRoot: RequestHandler = (req, res, next) => {
    if (req.isRoot !== 1) return res.status(403).send(responseMessage[403]);
    next();
};
