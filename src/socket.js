const { Server } = require('socket.io');
const config = require('./config');
const { logger } = require('./models/log.js');

module.exports = () => {
    const io = new Server();

    const { createClient } = require('redis');
    const { createAdapter } = require('@socket.io/redis-adapter');

    const pubClient = createClient(
        { url: `redis://:@${config.redis.host}:${config.redis.port}` }
    );
    const subClient = pubClient.duplicate();

    io.adapter(createAdapter(pubClient, subClient));


    io.of('/').on('connect', socket => {
        logger.info(`New Client ${socket.id} connected on WebSocket`);
        require('./socket/template.js')(socket);
    });

    return io;
};
